import React from 'react'
import Navbar from './components/_navbar'
import Container from '@mui/material/Container';
import profilePic from "../image/me3.JPG"
import Image from 'next/image'

export default function info() {
  return (
    <Container maxWidth="lg">
        <Navbar/>
        <div className="bg-white h-screen">
            <div className="gap-16 items-center py-8 px-4 mx-auto max-w-screen-xl lg:grid lg:grid-cols-2 lg:py-16 lg:px-6">
                <div className="grid gap-4 ">
                    <div
                        className="w-full max-w-sm border border-gray-200 rounded-lg shadow">
                        <div className="flex flex-col items-center py-10">
                            <Image className="w-36 h-36 mb-3 rounded-full shadow-lg object-cover" src={profilePic} alt="Natpacan" />
                            <h5 className="mb-1 text-xl font-medium text-gray-900 ">
                                Natpacan Sribanhad
                            </h5>
                            <span className="text-sm text-gray-500 ">Frontend Developer</span>
                                <a href="mailto:<%= port.contact.email %>"
                                    className="mt-4 inline-flex items-center px-4 py-2 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 ">
                                    natpacan.sri@gmail.com
                                </a>
                                <p className="mt-4 items-center px-4 text-sm font-medium text-center text-gray-900">(TH:+66) 093-374-1160</p>
                                <p className="mt-4 items-center px-4 text-sm font-medium text-center text-gray-900">Khonkaen University</p>
                        </div>
                    </div>
                </div>
                <div className="font-light text-gray-600 sm:text-lg">
                    <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-gray-900">
                        CAREER OBJECTIVE
                    </h2>
                    <p>
                        As a graduate in Information Technology. I'm seeking to be a full-time frontend developer. I aim to apply my knowledge and abilities to be a master frontend developer. And I am committed to diligently honing my skills, developing my abilities, and actively seeking new knowledge through continuous learning.
                    </p>
                </div>
            </div>
            <div className="items-center py-8 px-4 mx-auto max-w-screen-xl">
                <div className="font-light text-gray-500 sm:text-lg">
                    <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-gray-900">
                        Project Experience
                    </h2>
                    {/* <hr className="my-4"> */}
                    <ul className="text-gray-500 list-inside ">
                        <li className="flex items-center">
                            <svg className="mb-auto mt-2 w-3.5 h-3.5 mr-2 text-gray-500  flex-shrink-0" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                                <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z"/>
                            </svg>
                            <div>
                                <h2 className="font-bold">Mentor diamond web: <span className="font-light">Thesis & Research methodology project</span></h2>
                                <p>(Jun. 2023 - Present)</p>
                            </div>
                        </li>
                        <li className="flex items-center">
                            <svg className="w-3.5 h-3.5 mr-2 text-green-500  flex-shrink-0 mb-auto mt-2"
                                aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                viewBox="0 0 20 20">
                                <path
                                    d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
                            </svg>
                            <div>
                                <h2 className="font-bold">Rotten Potato app: <span className="font-light">Mobile Web Application Development</span></h2>
                                <p>(Dec. 2022 - Mar. 2023)</p>
                            </div>
                        </li>
                        <li className="flex items-center">
                            <svg className="w-3.5 h-3.5 mr-2 text-green-500 flex-shrink-0 mb-auto mt-2"
                                aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                viewBox="0 0 20 20">
                                <path
                                    d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
                            </svg>
                            <div>
                                <h2 className="font-bold">Turtle Typing web: <span className="font-light">Web Application Programming & Database Analysis and Design</span></h2>
                                <p>(Aug. 2022 - Oct. 2022)</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="items-center py-8 px-4 mx-auto max-w-screen-xl">
                <div className="font-light text-gray-500 sm:text-lg ">
                    <h2 className=" mb-4 text-4xl tracking-tight font-extrabold text-gray-900">
                        Education
                    </h2>
                    {/* <hr> */}
                    <h2 className="font-bold mt-4">Udonpichairakpittaya School</h2>
                    <p>Graduated in May 2020</p> 
                    <p>Present: <b>Bachelor of Science in Computing and Digital Technologies </b> - Khon Kaen University</p>
                </div>
            </div>
        </div>
    </Container>
  )
}
