/* eslint-disable react/jsx-max-props-per-line */
import Head from "next/head";
import {
  Box,
  Container,
  Unstable_Grid2 as Grid,
  Stack,
  Typography,
  Button,
  Card,
} from "@mui/material";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import { MemberList } from "src/components/memderList";
const workShopPage = () => {
  const listStuden = [
    {
      name: "ต๋อง",
      nameEn: "tong",
      path: "/workShop/tong",
    },
    {
      name: "แทน",
      nameEn: "puthaned",
      path: "/workShop/puthaned",
    },
    {
      name: "จอนนี่",
      nameEn: "natpacanSri",
      path: "/workShop/natpacanSri",
    },
    {
      name: "การ์ด",
      nameEn: "waiwitz",
      path: "/workShop/waiwitz",
    },
    {
      name: "เฟรชชี่",
      nameEn: "teerawut",
      path: "/workShop/teerawut",
    },
    {
      name: "อร",
      nameEn: "onrain",
      path: "/workShop/onrain",
    },
    {
      name: "แป้ง",
      nameEn: "meldyjuju",
      path: "/workShop/meldyjuju",
    },
    {
      name: "หมิว",
      nameEn: "chanoknan",
      path: "/workShop/chanoknan",
    },
    {
      name: "แบ๋ว",
      nameEn: "juthaporn",
      path: "/workShop/juthaporn",
    },
    {
      name: "ป๋อ",
      nameEn: "kunathip",
      path: "/workShop/kunathip",
    },
  ];
  return (
    <>
      <Head>
        <title>WORK SHOP</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Typography variant="h4">WorkShop</Typography>
          <Typography variant="subtitle1" mt={2}>
            List member {listStuden.length}
          </Typography>
          <MemberList memberList={listStuden} />
        </Container>
      </Box>
    </>
  );
};

workShopPage.getLayout = (workShopPage) => <DashboardLayout>{workShopPage}</DashboardLayout>;

export default workShopPage;
