import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, Button, Avatar, CircularProgress, Paper } from '@mui/material';
import { styled } from '@mui/system';
import Navbar from 'src/components/Por/Navbar';

const StyledContainer = styled(Container)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2em',
});

const UserInfoContainer = styled(Paper)({
  textAlign: 'center',
  marginTop: '1em',
  padding: '2em',
});

const AvatarImage = styled(Avatar)({
  width: '150px',
  height: '150px',
  margin: '0 auto 1em',
});

const LogoutButton = styled(Button)({
  marginTop: '1em',
});

const LoadingSpinner = styled(CircularProgress)({
  margin: '2em',
});

const User = () => {
  const router = useRouter();
  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const accessToken = localStorage.getItem('accessToken');
        if (!accessToken) {
          console.error('No access token found');
          return;
        }

        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/auth/user`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        if (response.data.status === 'ok') {
          setUser(response.data.user);
          localStorage.setItem('userToken', JSON.stringify(response.data.user));
          JSON.parse(localStorage.getItem('userToken'));
          console.log('User data:', response.data.user);
        } else {
          console.error('Error fetching user data:', response.data.message);
        }
      } catch (error) {
        console.error('API request error:', error);
      }
    };

    fetchUserData();
  }, []);

  const handleLogout = () => {
    localStorage.clear();
    router.push('/workShop/kunathip/workshop-login/login');
  };

  return (
    <>
    <Navbar/>
    <StyledContainer>
      <Typography variant="h4" gutterBottom>
        User Information
      </Typography>
      {user ? (
        <UserInfoContainer elevation={3}>
          <AvatarImage alt="User Avatar" src={user.avatar} />
          <Typography variant="h6" gutterBottom>
            ID: {user.id}
          </Typography>
          <Typography variant="h6" gutterBottom>
            Name: {user.fname} {user.lname}
          </Typography>
          <Typography variant="h6" gutterBottom>
            Email: {user.email}
          </Typography>
          <LogoutButton variant="contained" color="primary" onClick={handleLogout}>
            Logout
          </LogoutButton>
        </UserInfoContainer>
      ) : (
        <LoadingSpinner />
      )}
    </StyledContainer>
    </>
  );
};

export default User;
