import React from 'react'
import Navbarui2 from 'src/components/pang/navbarui2';
import Typography from "@mui/material/Typography";
import Link from "next/link";
import Footer from 'src/components/pang/footer';
import Footer2 from 'src/components/pang/footer3';
import Footer3 from 'src/components/pang/footer2';
import Footer4 from 'src/components/pang/footer4';
import Bodyportfolio from 'src/components/pang/bodyportfolio';
import Bodyportfolio2 from 'src/components/pang/bodyportfolio2';
import Bodyportfolio3 from 'src/components/pang/bodyportfolio3';
import { Box } from "@mui/material";


const portfolio = () => {
    return(
        <div>
                  <Navbarui2 />
                  <Bodyportfolio/>
                  <Bodyportfolio2/>
                  <Bodyportfolio3/> 
                  <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "200px",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-10em)",
            zIndex: 3, 
          }}
        >
          <Footer2 />
        </Box>
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-5em)",
            zIndex: 2, 
          }}
        >
          <Footer3 />
        </Box>
        <Box
        sx={{ 
          
        zIndex: 1, 
          
        }}><Footer4 /></Box>
        

      </Box>
        </div>
    );
};

export default portfolio;
