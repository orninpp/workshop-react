import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Link from 'next/link';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import styles from '/src/styles/orn/navwork2.module.css';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { styled, alpha } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';
import ArchiveIcon from '@mui/icons-material/Archive';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const NavbarWork2 = () => {
    
    const [anchorEl1, setAnchorEl1] = React.useState(null);
    const [anchorEl2, setAnchorEl2] = React.useState(null);
  
    const openMenu1 = Boolean(anchorEl1);
    const openMenu2 = Boolean(anchorEl2);
  
    const handleClick1 = (event) => {
      setAnchorEl1(event.currentTarget);
    };
  
    const handleClick2 = (event) => {
      setAnchorEl2(event.currentTarget);
    };
  
    const handleClose1 = () => {
      setAnchorEl1(null);
    };
  
    const handleClose2 = () => {
      setAnchorEl2(null);
    };
    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static" 
                    className={styles.navbg}>
                    <Toolbar>

                        <Typography variant="h6"
                            component="div"
                            sx={{ flexGrow: 1 }}>
                            HOMCO
                        </Typography>
                        <div>
                            <Button sx={{ color: 'inherit' }}>
                                <Link
                                    href="/workShop/onrain/workshop2"
                                    color="inherit">HOME</Link>
                            </Button>

                            <Button sx={{ color: 'inherit' }}>
                                <Link
                                    href="/workShop/onrain/workshop2"
                                    color="inherit">ABOUT US</Link>
                            </Button>

                            <Button sx={{ color: 'inherit' }}>
                                <Link
                                    href="/workShop/onrain/workshop2"
                                    color="inherit">OUR SERVICES</Link>
                            </Button>
                            <Button sx={{ color: 'inherit' , backgroundColor:'inherit'}}
                               id="demo-customized-button-1"
                               aria-controls={openMenu1 ? 'demo-customized-menu-1' : undefined}
                               aria-haspopup="true"
                               aria-expanded={openMenu1 ? 'true' : undefined}
                               variant="contained"
                               disableElevation
                               onClick={handleClick1}
                               endIcon={<KeyboardArrowDownIcon />}
                            >
                                 <Link
                                    href="/workShop/onrain/workshop2"
                                    color="inherit">OUR SERVICES</Link>
                                
                            </Button>
                            <Menu 
                                id="demo-customized-menu-1"
                                MenuListProps={{
                                  'aria-labelledby': 'demo-customized-button-1',
                                }}
                                anchorEl={anchorEl1}
                                open={openMenu1}
                                onClose={handleClose1}
                            >
                                <MenuItem onClick={handleClose1} disableRipple>
                                    Edit
                                </MenuItem>
                                <MenuItem onClick={handleClose1} disableRipple>
                                    
                                    Duplicate
                                </MenuItem>
                                <Divider sx={{ my: 0.5 }} />
                                <MenuItem onClick={handleClose1} disableRipple>
                                    
                                    Archive
                                </MenuItem>
                                <MenuItem onClick={handleClose1} disableRipple>
                                    
                                    More
                                </MenuItem>
                            </Menu>
                            <Button sx={{ color: 'inherit'}}>
                                <Link
                                    href="/workShop/onrain/workshop2"
                                    color="inherit">PORTFOLIO</Link>
                            </Button>
                            <Button sx={{ color: 'inherit', backgroundColor:'inherit'  }}
                               id="demo-customized-button-2"
                               aria-controls={openMenu2 ? 'demo-customized-menu-2' : undefined}
                               aria-haspopup="true"
                               aria-expanded={openMenu2 ? 'true' : undefined}
                               variant="contained"
                               disableElevation
                               onClick={handleClick2}
                               endIcon={<KeyboardArrowDownIcon />}
                            >
                                <Link
                                    href="#"
                                    color="inherit">PAGE</Link>
                            </Button>
                            <Menu
                                id="demo-customized-menu-2"
                                MenuListProps={{
                                  'aria-labelledby': 'demo-customized-button-2',
                                }}
                                anchorEl={anchorEl2}
                                open={openMenu2}
                                onClose={handleClose2}
                            >
                                <MenuItem onClick={handleClose2} disableRipple>
                                    Edit
                                </MenuItem>
                                <MenuItem onClick={handleClose2} disableRipple>
                                    Duplicate
                                </MenuItem>
                                <Divider sx={{ my: 0.5 }} />
                                <MenuItem onClick={handleClose2} disableRipple>
                                    Archive
                                </MenuItem>
                                <MenuItem onClick={handleClose2} disableRipple>   
                                    <Link href="/workShop/onrain/workshop2page2">
                                         FAQ
                                    </Link>               
                                    
                                </MenuItem>
                            </Menu>

                        </div>

                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <MenuIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </Box>

        </>
    )
}
export default NavbarWork2;