import Navbar from "./navbar"
import Head from "next/head"

import Box from '@mui/material/Box';
import { useState, useEffect } from "react";
import { Calculate } from "@mui/icons-material";

import styles from 'src/styles/orn/work.module.css'



const number = [1, 2, 3, 4, 5, 6, 7, 8, 9,'00', 0,'.'];
export default function Work() {
   
    let storedString = '';
    let result = '';

    const store = (item) => {
        storedString += item;
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = storedString;
    }

    const calculate = (storedString) => {
        try {
            result = eval(storedString);
        } catch (error) {
            result = "ไม่มีการกำหนดผลลัพธ์"
        }
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = result;
    }

    const reset = () =>{
        result = '';
        storedString = '';
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = storedString;
    }

    const reverse = () =>{
        let checknum = document.getElementById("result").innerHTML
        console.log(checknum)
        let newNum = 0
        if(checknum.substring(0,1) != '-'){
            if(checknum == storedString){
                storedString = "-"+storedString
                document.getElementById("result").innerHTML = storedString
            }else if(checknum == result){
                newNum = -result
            }
        }else if(checknum.substring(0,1) == '-'){
            if(checknum == storedString){
                storedString = storedString.slice(2)
                document.getElementById("result").innerHTML = storedString
            }else if(checknum == result){
                newNum = result.slice(2)
            }
        }
       
        
    }
    
   

    const percent = () =>{
        result = storedString / 100
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = result;
    }
    
    return (
        <>

            <Head>
                <title>ผลงาน | อร</title>
                <meta name='keywords'
                    content='My Work' />
            </Head>
            <Navbar />
            <div className="profileBg">
                 <div id="result" className={styles.result}>{storedString}</div>
                <Box className={styles.operateUp}>    
                        <button onClick={() => reset()}>C</button>
                        <button onClick={() => reverse()}>+/-</button>
                        <button onClick={() => percent()}>%</button>
                        <button onClick={() => store('/')}>/</button>
                </Box>
                <Box className={styles.boxCal}>
                    <div className={styles.number}>
                        {number.map((item) => (
                            <button key={item}
                                onClick={() => store(item)}>{item}</button>
                        ))}
                    </div>
                    <div className={styles.operate}>

                        <button onClick={() => store('+')}>+</button>
                        <button onClick={() => store('-')}>-</button>
                        <button onClick={() => store('*')}>*</button>
                        <button onClick={() => calculate(storedString)}>=</button>
                    </div>
                </Box>
            </div>
        </>
    )
}