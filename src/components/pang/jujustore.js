import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Rating from "@mui/material/Rating";

const TwoBoxesComponent = () => {
  const [workshopRating, setWorkshopRating] = useState(4);
  const [selectedWorkshop, setSelectedWorkshop] = useState(null);
  const workshopData = [
    { id: Math.random(), title: "พี่อู๋ให้ทำ resume 15/11/2023" },
    { id: Math.random(), title: "พี่อู๋ให้ทำ component 16/11/2023" },
    { id: Math.random(), title: "พี่อู๋ให้ทำ calculator-ช่วงบ่าย 16/11/2023" },
  ];
  const [newData, setNewData] = useState("");

  const handleRatingChange = (newValue) => {
    setWorkshopRating(newValue);
  };

  const handleDropdownChange = (event) => {
    const selectedId = event.target.value;
    const workshop = workshopData.find((item) => item.id === parseInt(selectedId, 10));
    setSelectedWorkshop(workshop);
  };

  const addDummyData = () => {
    if (newData.trim() !== "") {
      const newWorkshop = {
        id: workshopData.length + 1,
        title: newData,
      };
      setWorkshopData([...workshopData, newWorkshop]);
      setNewData("");
    }
  };

  return (
    <div style={{ display: "flex", justifyContent: "center", gap: "20px", padding: "20px" }}>
      <div
        style={{
          width: "250px",
          height: "250px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
        }}
      >
        <h2 style={{ color: "#000000", marginBottom: "10px", textAlign: "center", margin: "20px", padding: "5px" }}>
          WORKSHOP REVIEW
        </h2>
        <Typography variant="h3" style={{ color: "#000000", textAlign: "center" }}>
          GOOD
        </Typography>
        <Typography variant="h3" style={{ color: "#000000", textAlign: "center" }}>
          <Rating name="workshopRating" value={workshopRating} precision={0.5} readOnly />
        </Typography>
        <p style={{ color: "#000000" }}></p>
      </div>

      <div
        style={{
          width: "450px",
          height: "250px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
        }}
      >
        <h2 style={{ color: "#000000", marginBottom: "10px" }}>History my workshop </h2>

        <select
          onChange={handleDropdownChange}
          style={{
            width: "100%",
            padding: "10px",
            marginBottom: "10px",
            boxSizing: "border-box",
          }}
        >
          <option value={null}>Select to see my history</option>
          {workshopData.map((workshop) => (
            <option key={workshop.id} value={workshop.id}>
              {workshop.title}
            </option>
          ))}
        </select>

        <input
          type="text"
          style={{ width: "100%", padding: "10px", marginBottom: "10px", boxSizing: "border-box" }}
          placeholder="add history about workshop"
          value={newData}
          onChange={(e) => setNewData(e.target.value)}
        />
        
        <button
          style={{
            padding: "10px",
            marginTop: "10px",
            cursor: "pointer",
            backgroundColor: "#33691e",
            color: "white",
            border: "none",
            borderRadius: "8px",
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
          }}
          onClick={addDummyData}
        >
          ADD
        </button>
      </div>
    </div>
  );
};

export default TwoBoxesComponent;
