import { useState, useRef, useEffect } from "react";
import { Button, Menu, MenuItem } from "@mui/material";
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

export default () => {
  const [state, setState] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const menuRef = useRef(null);
  const [menuAnchorEl, setMenuAnchorEl] = useState(null);

  useEffect(() => {
    const handleOutsideClick = (e) => {
      if (menuRef.current && !menuRef.current.contains(e.target)) {
        setState(false);
      }
    };

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [menuRef]);

  const navigation = [
    { title: "resume", path: "/workShop/kunathip/workshop-resume" },
    { title: "workshop1", path: "/workShop/kunathip/workshop-calculator" },
    { title: "workshop2", path: "/workShop/kunathip/workshop-homco" },
    { title: "workshop3/4", path: "/workShop/kunathip/workshop-login/login" },
    { title: "workshop 5 user", path: "/workShop/kunathip/workshop-userapi/MediaCard" },
    { title: "workshop 5 search", path: "/workShop/kunathip/workshop-userapi/UserListPage" },
  ];

 
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuItemClick = (path) => {
    setAnchorEl(null);
    window.location.href = path;
  };

  return (
    <nav className="bg-white w-full border-b md:border-0 md:static">
      <div className="items-center px-4 max-w-screen-xl mx-auto md:flex md:px-8">
        <div className="flex items-center justify-between py-3 md:py-5 md:block">
          <a href="javascript:void(0)">
            <img
              src="https://firebasestorage.googleapis.com/v0/b/imagekunathip.appspot.com/o/logo.png?alt=media&token=2777c6c6-66cd-4b0c-9da7-d57622889750"
              width={60}
              height={25}
              alt="Float UI logo"
            />
          </a>
          <div className="md:hidden">
            <button
              className="text-gray-700 outline-none p-2 rounded-md focus:border-gray-400 focus:border"
              onClick={() => setState(!state)}
            >
              {state ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M4 8h16M4 16h16"
                  />
                </svg>
              )}
            </button>
          
            <Menu
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleClose}
              onClick={handleClose}
            >
              {navigation.map((item, idx) => (
                <MenuItem key={idx} onClick={() => handleMenuItemClick(item.path)}>
                  {item.title}
                </MenuItem>
              ))}
            </Menu>
          </div>
        </div>
        <div
          ref={menuRef}
          className={`flex-1 justify-self-center pb-5 mt-8 md:block md:pb-0 md:mt-0 ${
            state ? "block" : "hidden"
          }`}
        >
          <ul className="justify-center items-center space-y-8 md:flex md:space-x-1 md:space-y-0">
            {navigation.map((item, idx) => {
              return (
                <li key={idx} className="text-gray-600 hover:text-indigo-600">
                  <Button
                    variant="text"
                    color="inherit"
                    onClick={() => (window.location.href = item.path)}
                  >
                    {item.title}
                  </Button>
                </li>
              );
            })}
          </ul>
          
        </div>

        <div className="hidden md:inline-block">
          
          
          <Button
            onClick={() => (window.location.href = "/workShop/kunathip")}
          
            color="primary"
            variant="text"
            sx={{ borderRadius: "5px", backgroundColor: "#19a7ce", marginLeft: "10px" ,color: 'white'}}
          >
            หน้าแรก
          </Button>
        </div>
      </div>
    </nav>
  );
};
