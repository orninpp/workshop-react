import { Box, Button, Grid, Typography } from "@mui/material";
import ListText from "./listText";

const Introduce3 = ({ head1, head2, body }) => {
    return (
        <Grid container columns={{ xs: 4, sm: 8, md: 12 }} justifyContent={'center'}>
            <Grid item xs={6}>
                <Box sx={{ background: '#C4C4C4', width: '100%', height: '500px' }}>
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/FuXNumBwDOM?si=RTOzNFkfRh0OHZjO&amp;controls=0&amp;start=80" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Box sx={{ padding: '20px' }}>
                    <Typography sx={{ fontWeight: 'bold', color: '#757575' }}>{head1}</Typography>
                    <Typography variant="h3" sx={{ padding: '15px 0' }} gutterBottom>
                        {head2}
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        {body}
                    </Typography>
                    <ListText />
                </Box>
            </Grid>
        </Grid>
    )
}

export default Introduce3;