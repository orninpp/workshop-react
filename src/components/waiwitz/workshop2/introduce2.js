import { Box, Button, Grid, Typography } from "@mui/material";

const Introduce2 = ({ head1, head2, body }) => {
    return (
        <Grid container columns={{ xs: 4, sm: 8, md: 12 }} justifyContent={"space-between"}>
            <Grid item xs={6} margin={'10% 0'}>
                <Box sx={{ padding: '20px' }}>
                    <Typography sx={{ fontWeight: 'bold', color: '#757575' }}>{head1}</Typography>
                    <Typography variant="h3" sx={{ padding: '15px 0' }} gutterBottom>
                        {head2}
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        {body}
                    </Typography>
                    <Button sx={{
                        borderRadius: '0',
                        marginTop: '1em', backgroundColor: '#5f5f5f',
                        width: 'fit-content', color: '#FFFFFF', padding: '1.5em 4em'
                    }}>
                        PORTFOLIO
                    </Button>
                </Box>
            </Grid>
            <Grid item xs={4} zIndex={'-1'} margin={'auto'}>
                <Box sx={{ background: '#C4C4C4', width: '100%', height: '300px' }}></Box>
                <Box sx={{ background: '#DADADA', width: '100%', height: '300px', transform: ' translateY(-150px) translateX(-80px)', zIndex: '2' }}></Box>
                <Box sx={{ background: '#C4C4C4', width: '100%', height: '300px', transform: ' translateY(-250px)' }}></Box>
            </Grid>
        </Grid>
    )
}

export default Introduce2;