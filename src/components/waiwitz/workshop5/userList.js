import { Button, Box, Typography, Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper } from "@mui/material";
import { useEffect } from "react";

const UserList = ({ users, openEdit, openDelete, setUsers }) => {

    useEffect(() => {
        const fetchAllUser = () => {
            fetch(`${process.env.NEXT_PUBLIC_USERLIST_APIUSERS}`, {
                method: `GET`,
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(async (res) => {
                try {
                    const data = await res.json();
                    if (res.ok) {
                        if (data.message) console.info(data.message);
                        setUsers(data)
                    } else {
                        if (data.message) console.info(data.message);
                        console.error('Status : ', res.statusText);
                    }
                } catch (error) {
                    console.error('Error : ', error);
                }
            });
        }
        const intervalId = setInterval(fetchAllUser, 2000);
        return () => clearInterval(intervalId);
    }, [])
    return (
        <>
            <Box sx={{ overflow: 'scroll', height: '60vh' }}>
                <Typography gutterBottom variant="h4">
                    Update User
                </Typography>
                {
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((user, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            <img src={user.avatar} width={50} height={50} />
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {user.username}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {user.fname} {user.lname}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            <Button onClick={() => openEdit(user)}>
                                                Edit
                                            </Button>
                                            <Button onClick={() => openDelete(user)} >
                                                Delete
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                }
            </Box>
        </>
    )
}

export default UserList;