import React, { useState, useEffect } from "react";
import {
    Button,
    MenuItem,
    Select,
    CircularProgress,
    Snackbar,
    Alert,
    Box,
    Typography,
    FormControl,
    InputLabel,
} from "@mui/material";
import { Circle as CircleIcon } from '@mui/icons-material';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const UserDelete = ({ onDelete }) => {
    const [deleting, setDeleting] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState("");
    const [userList, setUserList] = useState([]);
    const [selectedUserId, setSelectedUserId] = useState("");
    const [selectedUser, setSelectedUser] = useState(null);

    useEffect(() => {
        const fetchUserList = async () => {
            try {
                const response = await fetch("https://www.melivecode.com/api/users");
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const result = await response.json();
                setUserList(result);
            } catch (error) {
                console.error("Error fetching user list:", error);
            }
        };

        fetchUserList();
    }, [userList]);

    const handleDelete = async () => {
        setDeleting(true);

        try {
            const response = await fetch("https://www.melivecode.com/api/users/delete", {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    id: selectedUserId,
                }),
            });

            const result = await response.json();

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}, Message: ${result.message}`);
            }

            setSnackbarMessage(result.message);
            setSnackbarOpen(true);
        } catch (error) {
            setSnackbarMessage(error.message);
            setSnackbarOpen(true);
        } finally {
            setDeleting(false);
            // Reset state
            setSelectedUserId("");
            setSelectedUser(null);
            setUserList([]);
        }
    };

    const handleUserSelect = (event) => {
        const selectedUser = userList.find(user => user.id === event.target.value);
        setSelectedUserId(event.target.value);
        setSelectedUser(selectedUser);
    };

    return (
        <>
            <FormControl variant="outlined" sx={{ my: 2 }}>
                <InputLabel>User List</InputLabel>
                <Select
                    name="user"
                    value={selectedUserId}
                    MenuProps={MenuProps}
                    sx={{minWidth: '20vw'}}
                    renderValue={(selected) => (
                        selected ? (
                            <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
                                <img src={selectedUser?.avatar} style={{ width: 32, height: 32, borderRadius: '50%' }} alt={selectedUser?.fname} />
                                <Typography variant="button">
                                    {`${selectedUser?.fname} ${selectedUser?.lname}`}
                                </Typography>
                            </Box>
                        ) : (
                            <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
                                <Typography variant="button">
                                    Choose a user!
                                </Typography>
                            </Box>
                        )
                    )}
                    onChange={handleUserSelect}
                >
                    {userList.map((user) => (
                        <MenuItem key={user.id} value={user.id} sx={{ display: 'flex', flexDirection: 'row', gap: 2 }}>
                            <img src={user.avatar} style={{ width: 32, height: 32, borderRadius: '50%' }} alt={user.fname} />
                            <Typography variant="button">
                                {`${user.fname} ${user.lname}`}
                            </Typography>
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>

            {selectedUser && (
                <Box sx={{ display: 'flex', gap: 4, mt: 4 }}>
                    <Box sx={{ position: 'relative' }}>
                        <img src={selectedUser.avatar} alt='Avatar' style={{
                            width: '120px', height: '120px',
                            borderRadius: '50%', outline: '1px solid #62626240',
                            outlineOffset: '5px'
                        }} />
                        <CircleIcon sx={{ position: 'absolute', right: 0, bottom: 6, color: '#23c369', border: '1px solid #62626240', outline: '3px solid white', borderRadius: '50%', outlineOffset: -4, fontSize: '1.75em' }} />
                    </Box>
                    <Box>
                        <Typography variant="button">{`ID: ${selectedUser.id}`}</Typography>
                        <Typography variant="h6" mb={1}>{`Name: ${selectedUser.fname} ${selectedUser.lname}`}</Typography>
                        <Typography variant="body1">{`Username: ${selectedUser.username}`}</Typography>
                        <Typography variant="body1">{`Email: ${selectedUser.username}`}</Typography>
                    </Box>
                </Box>
            )}

            <Button
                variant="outlined"
                color="error"
                onClick={handleDelete}
                disabled={deleting || !selectedUserId}
                sx={{mt:4}}
            >
                {deleting ? <CircularProgress size={24} /> : "Delete User"}
            </Button>
            <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={() => setSnackbarOpen(false)}>
                <Alert onClose={() => setSnackbarOpen(false)} severity="success" sx={{ width: "100%" }}>
                    {snackbarMessage}
                </Alert>
            </Snackbar>
        </>
    );
};

export default UserDelete;
