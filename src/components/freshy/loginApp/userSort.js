import { Box, FormControl, InputLabel, MenuItem, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useEffect, useState } from "react";

const UserSort = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [sortColumn, setSortColumn] = useState("id");
    const [sortOrder, setSortOrder] = useState("desc");

    const fetchData = async () => {
        setLoading(true);

        try {
            const response = await fetch(`https://www.melivecode.com/api/users?sort_column=${sortColumn}&sort_order=${sortOrder}`);
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            const result = await response.json();

            console.info('[INFO] Fetched user sort successfully')
            console.log('UserSort', result);
            setData(result);
        } catch (error) {
            console.error("Error fetching data:", error);
        } finally {
            setLoading(false);
        }
    };

    const handleSortColumnChange = (event) => {
        setSortColumn(event.target.value);
    };

    const handleSortOrderChange = (event) => {
        setSortOrder(event.target.value);
    };

    useEffect(() => {
        fetchData();
    }, [sortColumn, sortOrder]);

    return (
        <>
            <Box sx={{ display: 'flex', gap: 2, justifyContent: 'end', mt: 2 }}>
                <FormControl>
                    <InputLabel htmlFor="sort-column">Sort Column</InputLabel>
                    <Select
                        value={sortColumn}
                        onChange={handleSortColumnChange}
                        inputProps={{
                            name: "sort-column",
                            id: "sort-column",
                        }}
                    >
                        <MenuItem value="id">ID</MenuItem>
                        <MenuItem value="fname">First Name</MenuItem>
                        <MenuItem value="lname">Last Name</MenuItem>
                        <MenuItem value="username">Username</MenuItem>
                        {/* Add more options for other columns */}
                    </Select>
                </FormControl>

                <FormControl>
                    <InputLabel htmlFor="sort-order">Sort Order</InputLabel>
                    <Select
                        value={sortOrder}
                        onChange={handleSortOrderChange}
                        inputProps={{
                            name: "sort-order",
                            id: "sort-order",
                        }}
                    >
                        <MenuItem value="asc">Ascending</MenuItem>
                        <MenuItem value="desc">Descending</MenuItem>
                    </Select>
                </FormControl>
            </Box>
            <TableContainer component={Paper} style={{ marginTop: 20 }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Avatar</TableCell>
                            <TableCell>First Name</TableCell>
                            <TableCell>Last Name</TableCell>
                            <TableCell>Username</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <TableRow>
                                <TableCell colSpan={5}>Loading...</TableCell>
                            </TableRow>
                        ) : (
                            data.map((user) => (
                                <TableRow key={user.id}>
                                    <TableCell>{user.id}</TableCell>
                                    <TableCell>
                                        <img src={user.avatar} alt={`Avatar of ${user.Iname}`} style={{ width: 50, height: 50, borderRadius: "50%" }} />
                                    </TableCell>
                                    <TableCell>{user.fname}</TableCell>
                                    <TableCell>{user.lname}</TableCell>
                                    <TableCell>{user.username}</TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
};

export default UserSort;