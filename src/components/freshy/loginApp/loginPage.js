// LoginPage.js
import React, { useState, useEffect } from 'react';
import {
    TextField,
    Button,
    Snackbar,
    Container,
    Box,
    Typography,
    Alert,
    Paper,
    FormHelperText,
    CircularProgress,
} from '@mui/material';

const LoginPage = ({ handleUser, setOpenSnackbar, snackbarMessage, setSnackbarMessage }) => {
    const storedUserData = localStorage.getItem('userData');
    const userData = storedUserData ? JSON.parse(storedUserData) : null;
    const [username, setUsername] = useState(null || userData?.username);
    const [password, setPassword] = useState(null || userData?.password);
    const [loading, setLoading] = useState(false);

    const handleLogin = async () => {
        try {
            setLoading(true);
            const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/login`, {
                // Use the environment variable here
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username,
                    password,
                    expiresIn: 60000,
                }),
            });

            const data = await response.json();

            if (response.ok) {
                // Successful login
                localStorage.setItem('accessToken', data.accessToken);
                localStorage.setItem('userData', JSON.stringify(data.user)); // Store user data
                setSnackbarMessage({ status: 'success', title: 'Login successful' });
                console.info('[INFO] Login successful');
                fetchUserData(data.accessToken);
            } else {
                // Login failed
                setSnackbarMessage({ status: 'failed', title: 'Login failed' });
                console.warn('[WARNING] Login failed');
            }

            setOpenSnackbar(true);
        } catch (error) {
            console.error('[ERROR] Error during login: ', error);
        } finally {
            setLoading(false);
        }
    };

    const fetchUserData = async (accessToken) => {
        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/auth/user`, {
                // Use the environment variable here
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });

            const userData = await response.json();

            if (response.ok) {
                // User details fetched successfully
                handleUser(userData.user);
                console.info("[INFO] Fetch userData successfully")
            } else {
                // Unable to fetch user details
                console.error('[ERROR] Unable to fetch user details');
            }
        } catch (error) {
            console.error('[ERROR] Error fetching user details:', error);
        }
    };

    useEffect(() => {
        // Check if the user is already logged in with a valid access token
        const accessToken = localStorage.getItem('accessToken');
        if (accessToken) {
            fetchUserData(accessToken);
            console.info(`[INFO] ${userData.fname} ${userData.lname} is already logged in with a valid access token`)
        }
    }, []);

    return (
        <Box
            sx={{
                display: 'flex',
                gap: 4,
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                height: '100vh',
                bgcolor: '#EDF3F8',
            }}
        >
            <Paper
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 4,
                    justifyContent: 'center',
                    p: 6,
                    minWidth: '380px',
                    borderRadius: 2,
                }}
            >
                <Typography variant='h5'>Login</Typography>
                <TextField
                    label='Username'
                    variant='outlined'
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <TextField
                    label='Password'
                    variant='outlined'
                    type='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <Button variant='contained' color='primary' onClick={handleLogin} disabled={loading}>
                    {loading ? <CircularProgress size={24}/> : 'Login'}
                </Button>
                <FormHelperText error>
                    {snackbarMessage.status === 'failed' && 'Incorrect username or password'}
                </FormHelperText>
            </Paper>
        </Box>
    );
};

export default LoginPage;
