import { Box, Grid, List, ListItem, ListItemIcon, ListItemText, Paper, Typography } from "@mui/material";
import Image from "next/image";
import InteriorPhoto4 from 'public/assets/freshy/designUI/interior4.jpg'
import { cloneElement, useState } from "react";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CheckListItem from "./intro/checklist";
import InsightItem from "./intro/insight";

const introductionObject = {
    intro: "WHO ARE WE",
    title: "WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION",
    subtitle: `Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 
      totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
      Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, 
      sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.`,
    checkList: [
        { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
        { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
        { title: "Client Priority", startIcon: <CheckCircleIcon /> },
        { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
        { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
        { title: "Client Priority", startIcon: <CheckCircleIcon /> },
    ],
    insight: [
        { title: "15Y", subtitle: "EXPERIENCE" },
        { title: "25+", subtitle: "BEST TEAM" },
        { title: "500+", subtitle: "TOTAL CLIENT" },
    ],
};


const Introduction = () => {
    return (
        <Paper
            elevation={0}
            sx={{
                display: "flex",
                justifyContent: "center",
                fontFamily: "Inter, sans-serif",
                flexDirection: { xs: "column", md: "row" },
                my: { xs: 12, md: 24 },
                gap: 4,
            }}
        >
            <Box flex={1}>
                <Image
                    src={InteriorPhoto4}
                    style={{
                        height: "100%",
                        width: "100%",
                        objectFit: "cover",
                        borderRadius: 12,
                    }}
                    alt="Interior"
                />
            </Box>
            <Box flex={1}>
                <Typography variant="overline" color="initial" sx={{ my: 2 }}>
                    {introductionObject.intro}
                </Typography>
                <Typography
                    variant="h1"
                    color="initial"
                    sx={{
                        my: 2,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }}
                >
                    {introductionObject.title}
                </Typography>
                <Typography
                    variant="subtitle1"
                    color="initial"
                    fontWeight="normal"
                    sx={{ my: 2 }}
                >
                    {introductionObject.subtitle}
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <List>
                            {introductionObject.checkList.slice(0, 3).map((item, index) => (
                                <CheckListItem
                                    key={index}
                                    startIcon={item.startIcon}
                                    primary={item.title}
                                    iconColor='#3e99ec'
                                />
                            ))}
                        </List>
                    </Grid>
                    <Grid item xs={6}>
                        <List>
                            {introductionObject.checkList.slice(3).map((item, index) => (
                                <CheckListItem
                                    key={index}
                                    startIcon={item.startIcon}
                                    primary={item.title}
                                    iconColor='#3e99ec'
                                />
                            ))}
                        </List>
                    </Grid>
                </Grid>
                <Grid container spacing={2} my={1}>
                    {introductionObject.insight.map((item, index) => (
                        <InsightItem
                            key={index}
                            title={item.title}
                            subtitle={item.subtitle}
                        />
                    ))}
                </Grid>
            </Box>
        </Paper>
    );
};

export default Introduction;