import React, { useEffect, useState } from 'react'
import CLayout from '../cLayout';
import AttrTable from '../attrTable';


export default function language() {

    const [attrData, setAttrData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [lang, setLang] = useState('th')

    useEffect(()=> {
        const fetchData = async () => {
            try {
                
                const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/${lang}/attractions`);
                const res = await response.json();
                const attrData = res.data;
                setAttrData(attrData);
                        
                console.log("data:", res)
                console.log("attr:", attrData)
                
                if(userData.length > 0){
                    setLoading(false);
                }

            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        }
        fetchData()
    },[lang])



    return (
        <CLayout>
            
        </CLayout>
    )
}
