import React from 'react';
import TablePagination from '@mui/material/TablePagination';

export default function TablePaginationDemo({ count, page, onPageChange, rowsPerPage, onRowsPerPageChange }) {
  const handleChangePage = (event, newPage) => {
    onPageChange(event, newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    onRowsPerPageChange(event);
  };

  return (
    <TablePagination
      component="div"
      count={count}
      page={page}
      onPageChange={handleChangePage}
      rowsPerPage={rowsPerPage}
      onRowsPerPageChange={handleChangeRowsPerPage}
    />
  );
}
