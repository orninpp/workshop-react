import { Container, Box, Typography } from '@mui/material'
// import { Box } from '@mui/system'
import React from 'react'

export default function banner({ titleName }) {
    return (
        <Container
            maxWidth='full'
            sx={{ 
                height:'350px',
                backgroundColor: "#19A7CE",
                paddingTop:"100px",
                paddingRight: "24px", 
                paddingLeft: "24px",
                position:"relative",
                zIndex:'20',
                marginBottom:"80px"
            }}>
            {/*  */}
            <Box
                sx={{
                    width: "8rem",
                    height: "8rem",
                    border: "16px solid #146C94",
                    borderRadius: "100%",
                    position:"absolute",
                    zIndex:"-10",
                    top:"50",
                    bottom:"50",
                }}
            ></Box>
            {/*  */}
            <Box
                sx={{
                    position:"absolute",
                    width:"500px",
                    zIndex:"10",
                    top:"50",
                    bottom:"50",
                    // border:"1px solid white"
                }}
                >
                <Typography
                    sx={{
                        fontWeight: "bold",
                        fontSize: '3.75rem',
                        color: 'white',
                    }}
                >{titleName}</Typography>
                <Typography
                    sx={{
                        fontWeight:"600",
                        color:"white",
                        marginTop:'-8px'
                    }}
                >lorst amet, consectetur ad p sc ng el t. Ut el t tellus, uctus
                    psunec ullamcor er mattis, ulvinar dapibus leo.</Typography>
            </Box>
        </Container>
    )
}
