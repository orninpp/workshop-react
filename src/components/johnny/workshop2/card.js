import React from 'react'

import {
    Card,
    CardContent,
    CardMedia,
    Typography,
    Button
} from '@mui/material';



export default function card({cardPic,cardTitle,cardDetail}) {
    return (
        <Card 
            elevation={0}
            sx={{ 
                maxWidth: 400,
                margin:"0 auto",
                borderRadius: 0,
                boxShadow:'none',
                borderBottom:'2px solid black',
                borderLeft:'2px solid black',
                marginBottom:'60px'
            }}>
            <CardMedia
                sx={{ height: 300 }}
                image={cardPic}
                title="card pic"
            />
            <CardContent
                sx={{
                    backgroundColor:""
                }}
            >
                <Typography gutterBottom variant="h5" component="div">
                    {cardTitle}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {cardDetail}
                </Typography>
            </CardContent>
        </Card>
    )
}
