import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

export default function MenuPopupState() {
  return (
    <PopupState variant="popover" popupId="demo-popup-menu">
      {(popupState) => (
        <React.Fragment>
          <Button variant="contained" {...bindTrigger(popupState)}
            sx={{
              backgroundColor: "#19A7CE",
              boxShadow: "none",
              "&:hover": {
                backgroundColor: "#46b8d7",
              },
            }} >
            Our Project &#9660;
          </Button>
          <Menu {...bindMenu(popupState)} sx={{}}>
            <MenuItem onClick={popupState.close}>Project 1</MenuItem>
            <MenuItem onClick={popupState.close}>Project 2</MenuItem>
            <MenuItem onClick={popupState.close}>Project 3</MenuItem>
          </Menu>
        </React.Fragment>
      )}
    </PopupState>
  );
}