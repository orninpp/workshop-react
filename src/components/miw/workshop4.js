import React, { useEffect,useState } from "react";
import { Button, TextField, Container, Typography } from "@mui/material";
import { useRouter } from "next/navigation";


const Workshop4 = () => {
  const [userData, setUserData] = useState(null);
  const {push} = useRouter()


  useEffect(()=>{

    const showData = async()=>{
        const userResponse = await fetch("https://www.melivecode.com/api/auth/user", {
            method: "GET",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("jwt")}`,
            },
          });
    
          if (!userResponse.ok) {
              const errorData = await userResponse.json(); // Assuming the server sends JSON error details
              throw new Error(`Failed to fetch user data: ${userResponse.status} - ${errorData.message}`);
              
            }
            const userData = await userResponse.json();
            localStorage.setItem("workshop4", JSON.stringify(userData));
            setUserData(userData)
        }

        showData()

  },[])

  const  logOut = () =>{
    localStorage.removeItem("jwt")
    localStorage.removeItem("workshop4")
    push("/workShop/chanoknan/workshop3")
  }
  
    


  return (
    <div>
      {userData && (
        <div>
          <Typography variant="h6">User Information:</Typography>
          <Typography>Username: {userData.user.username}</Typography>
          <Typography>Email: {userData.user.email}</Typography>
          <Typography>First Name: {userData.user.fname}</Typography>
          <Typography>Last Name: {userData.user.lname}</Typography>
          {/* Display user avatar */}
          {userData.user.avatar && (
            <img
              src={userData.user.avatar}
              alt="User Avatar"
              style={{ width: "300px", height: "300px" }}
            />
          )}

          <Button onClick={logOut}>Logout</Button>
        </div>
      )}
    </div>
  );
          }

export default Workshop4;
