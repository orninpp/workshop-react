import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import WidgetsIcon from '@mui/icons-material/Widgets';


const Navui = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar position="static" sx={{ backgroundColor: "#9AA090 " }}>
      <Toolbar>
        <Typography variant="h6" sx={{ flexGrow: 1, fontSize: "2rem" }}>
          <WidgetsIcon
          sx={{ fontSize: '2.5rem' }}/>
          HOMCO
        </Typography>
        <Button color="inherit" >
          HOME
        </Button>
        <Button color="inherit" >
          ABOUT ME
        </Button>
        <Button color="inherit" >
          OUR SERVICES
        </Button>
        <Button color="inherit" onClick={handleClick}>
          OUR PROJECTS
          <IconButton
            size="small"
            edge="end"
            aria-label="dropdown"
            aria-controls="dropdown-menu"
            aria-haspopup="true"
            onClick={handleClick}
            color="inherit"
          >
            <ExpandMoreIcon />
          </IconButton>
        </Button>
        <Button color="inherit" >
          PORTFOLIO
        </Button>

        <Button color="inherit" onClick={handleClick}>
          PAGES
          <IconButton
            size="small"
            edge="end"
            aria-label="dropdown"
            aria-controls="dropdown-menu"
            aria-haspopup="true"
            onClick={handleClick}
            color="inherit"
          >
            <ExpandMoreIcon />
          </IconButton>
        </Button>
        <Menu id="dropdown-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
          <MenuItem onClick={handleClose}>Dropdown Item 1</MenuItem>
          <MenuItem onClick={handleClose}>Dropdown Item 2</MenuItem>
          <MenuItem onClick={handleClose}>Dropdown Item 3</MenuItem>
        </Menu>
        <MenuIcon />
      </Toolbar>


    </AppBar>
  );
};

export default Navui;
